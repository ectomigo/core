#!/usr/bin/env node

'use strict';

import _ from 'lodash';
import {program} from 'commander';
import indexFiles from '../lib/indexer/files.js';
import match from '../lib/matcher/index.js'
import axios from 'axios';
import {globby} from 'globby';
import {basename, resolve} from 'path';
import FormData from 'form-data';
import {Readable, Transform} from 'stream';
import {pipeline} from 'stream/promises';
import {createReadStream, createWriteStream} from 'fs';
import {readFile} from 'fs/promises';

const BASE_URL = 'http://lamia:3000';
const DEFAULT_IGNORE = ['tags'];

program
  .command('index <dir>')
  .option('-o, --owner [name]', 'repo owner name [test]', 'test')
  .option('-f, --ref [name]', 'branch/tag name [master]', 'master')
  .option('-b, --main-branch', 'flag this job as main to consider its results from other processing jobs')
  .option('-i, --ignore <path>', 'ignore paths', (val, acc) => acc.concat([val]), [])
  .option('-m, --migrations <path>', 'directory containing schema migration scripts')
  .option('-p, --patterns <json>', 'map named data access patterns to glob arrays: {"pojo": ["src/main/java/com/dot/models/**/*"]}')
  .description('index a directory')
  .action(async function (dir, options) {
    const pkg = await readFile('./package.json');
    const absolutePath = resolve(dir);
    const name = basename(dir);
    const {data: job} = await axios.post(`${BASE_URL}/jobs`, {
      name: options.owner,
      repo: name,
      patterns: options.patterns ? JSON.parse(options.patterns) : null,
      ignore_paths: options.ignore ? _.castArray(options.ignore) : null,
      migration_paths: options.migrations ? _.castArray(options.migrations) : null,
      url: dir,
      ref: options.ref,
      main_branches: options.mainBranch ? [options.ref] : [],
      platform: 'core',
      module_version: JSON.parse(pkg).version,
      run_id: 123
    });

    const files = await globby(
      _.concat(
        // all files....
        [`${absolutePath}/**/*`],
        // ....except anything in ignore or migration paths
        _.concat(
          DEFAULT_IGNORE,
          job.ignore_paths || [],
          job.migration_paths || []
        ).map(p => `!${absolutePath}/**/${p}`)
      ),
      {
        gitignore: true,
        cwd: absolutePath
      }
    );

    const transform = Transform({
      objectMode: true,
      transform: function (inv, enc, next) {
        next(null, `${JSON.stringify(inv)}\n`);
      }
    });
    transform.path = 'totally a file'; // form-data wants a path and fortunately isn't picky

    const form = new FormData();

    form.append('platform', 'core');
    form.append('token', job.job_id);
    form.append('invocations', Readable.from(indexFiles(job, dir, files)).pipe(transform));

    const {data: count} = await axios.post(`${BASE_URL}/upload`, form, {
      headers: form.getHeaders()
    });

    console.log(`indexed ${count} database invocations in ${name}/${options.ref}`);
    console.log(`job token is ${job.job_id}`);

    process.exit(0);
  });

program
  .command('process <file> [files...]')
  .requiredOption('-t, --token <token>', 'a job token')
  .description('process migration scripts')
  .action(async function (file, files, options) {
    const allFiles = [file].concat(files);
    const migrations = await match(null, allFiles);
    console.log(migrations)

    const {data: invocations} = await axios.post(BASE_URL, {
      platform: 'core',
      token: options.token,
      migrations
    });

    const acc = ['# ectomigo results'];
    const actions = {
      alter_table: 'altered',
      drop_table: 'dropped',
      alter_view: 'altered',
      drop_view: 'dropped'
    };

    console.log(invocations);

    const byEntity = _.groupBy(invocations, i => i.entity);

    for (const entity in byEntity) {
      acc.push('');
      acc.push(`## ${entity}`);

      // first pass: print location of changes in migrations
      const changes = byEntity[entity].reduce((dict, record) => {
        if (!dict.hasOwnProperty(record.file_name)) {
          dict[record.file_name] = {};
        }

        for (const c of record.change) {
          if (!dict[record.file_name].hasOwnProperty(c.kind)) {
            dict[record.file_name][c.kind] = new Set();
          }

          dict[record.file_name][c.kind].add(c.y1);
        }

        return dict;
      }, {});

      const prefix = changes.length > 1 ? '* ' : '';

      for (const filename in changes) {
        for (const type in changes[filename]) {
          const lines = [...changes[filename][type]];

          if (lines.length === 1) {
            acc.push(`${prefix}${actions[type]} on line ${lines.join(', ')} of ${filename}`);
          } else {
            acc.push(`${prefix}${actions[type]} on lines ${lines.join(', ')} of ${filename}`);
          }
        }
      }

      // second pass: print references
      for (const record of byEntity[entity]) {
        acc.push('');
        acc.push(`### ${record.repo}/${record.ref}`);

        let fileName;

        for (const inv of record.invocations) {
          if (fileName !== inv.file_path) {
            fileName = inv.file_path;

            acc.push(`* ${record.repo}: ${inv.file_path}`);
          }

          // TODO look for column matches to alters/ellipsize/emoji code for
          // confidence? we can't color text unfortunately
          const columns = inv.is_all_columns ? 'all columns' : _.sortBy(inv.column_refs, r => r.confidence)
            .map(r => r.name)
            .join(', ');

          acc.push(`  - line ${inv.y1} (${columns})`);
        }
      }
    }

    console.log(acc.join('\n'));

    process.exit(0);
  });

program.parse(process.argv);
