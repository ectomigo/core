# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.1.4](https://gitlab.com/ectomigo/core/compare/v0.1.3...v0.1.4) (2022-04-28)

### [0.1.3](https://gitlab.com/ectomigo/core/compare/v0.1.2...v0.1.3) (2022-04-28)


### Features

* send platform & version with job ([223c66d](https://gitlab.com/ectomigo/core/commit/223c66d8bb73b7fa4cff59aea364d92acf87e6e4))

### [0.1.2](https://gitlab.com/ectomigo/core/compare/v0.1.1...v0.1.2) (2022-03-26)


### Features

* handle javascript and python string interpolation ([c20a7a9](https://gitlab.com/ectomigo/core/commit/c20a7a9f55eeae53a4d970faaacc31f1c537c532))

### [0.1.1](https://gitlab.com/ectomigo/core/compare/v0.1.0...v0.1.1) (2022-03-15)


### Features

* index sqlalchemy Tables and ORM classes ([afc8446](https://gitlab.com/ectomigo/core/commit/afc84465e25ef9cfec097fe62b836bf50d6fa197))
* track main branches while indexing ([6b73435](https://gitlab.com/ectomigo/core/commit/6b734359c7e7822de5e16962fd678c9fbcd29f11))

### [0.1.1](https://gitlab.com/ectomigo/core/compare/v0.1.0...v0.1.1) (2022-03-15)


### Features

* index sqlalchemy Tables and ORM classes ([afc8446](https://gitlab.com/ectomigo/core/commit/afc84465e25ef9cfec097fe62b836bf50d6fa197))
* track main branches while indexing ([6b73435](https://gitlab.com/ectomigo/core/commit/6b734359c7e7822de5e16962fd678c9fbcd29f11))

### [0.1.1](https://gitlab.com/ectomigo/core/compare/v0.1.0...v0.1.1) (2022-03-15)


### Features

* index sqlalchemy Tables and ORM classes ([afc8446](https://gitlab.com/ectomigo/core/commit/afc84465e25ef9cfec097fe62b836bf50d6fa197))
* track main branches while indexing ([6b73435](https://gitlab.com/ectomigo/core/commit/6b734359c7e7822de5e16962fd678c9fbcd29f11))

### [0.1.2](https://gitlab.com/ectomigo/core/compare/v0.1.0...v0.1.2) (2022-03-15)


### Features

* index sqlalchemy Tables and ORM classes ([afc8446](https://gitlab.com/ectomigo/core/commit/afc84465e25ef9cfec097fe62b836bf50d6fa197))
* track main branches while indexing ([6b73435](https://gitlab.com/ectomigo/core/commit/6b734359c7e7822de5e16962fd678c9fbcd29f11))

### [0.1.1](https://gitlab.com/ectomigo/core/compare/v0.1.0...v0.1.1) (2022-03-15)


### Features

* index sqlalchemy Tables and ORM classes ([afc8446](https://gitlab.com/ectomigo/core/commit/afc84465e25ef9cfec097fe62b836bf50d6fa197))
* track main branches while indexing ([6b73435](https://gitlab.com/ectomigo/core/commit/6b734359c7e7822de5e16962fd678c9fbcd29f11))
